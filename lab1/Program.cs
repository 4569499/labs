﻿using System.Numerics;


static void DefineCiphAlgo()
{
    Console.WriteLine(@"Choose cipher algoritm type
    1 - Caeser
    2 - Linear
    3 - Affine");

    switch (Console.ReadKey().Key)
    {
        case ConsoleKey.D1:
            CaeserCipher.Encode(FileWorker.LoadFile());
            break;
        case ConsoleKey.D2:
            LinearCipher.Encode(FileWorker.LoadFile());
            break;
        case ConsoleKey.D3:
            AffCipher.Encode(FileWorker.LoadFile());
            break;
        default:
            Console.WriteLine("Not a vaild crypto algoritm type");
            DefineCiphAlgo();
            break;
    }
}

Console.Write("Source file name= ");
FileWorker.ReadPath = Console.ReadLine();
Console.Write("Output file name= ");
FileWorker.WritePath = Console.ReadLine();
DefineCiphAlgo();

// Delay
Console.ReadKey(false);

static class FileWorker
{
    public static string? ReadPath { get; set; }
    public static string? WritePath { get; set; }
    public static string LoadFile()
    {
        using StreamReader sr = new(Environment.CurrentDirectory + "/" + ReadPath + ".txt"); 
        return sr.ReadToEnd().ToUpper();
    }

    public static void SaveFile(string fileContent)
    {
        using StreamWriter sw = new(Environment.CurrentDirectory + "/" + WritePath + ".txt", true);
        sw.WriteLine(fileContent);
    }
}

struct CiphConstants
{
    public static readonly string alphabet = "АБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЬЮЯ _,.";
    /// <summary>
    /// Length of alphabet
    /// </summary>
    public static readonly int m = alphabet.Length;
    /// <summary>
    /// K for Cesar 
    /// </summary>
    public static readonly int cesarK = 17;
    /// <summary>
    /// K for linear
    /// </summary>
    public static readonly int linearK = 18;
    /// <summary>
    /// Derivative linear K, computed as ModPow(k,-1)
    /// </summary>
    public static readonly int linearKDerr = AlgoHelper.ModInverse(linearK);
    /// <summary>
    /// K for affine
    /// </summary>
    public static readonly int affK = 15;
    /// <summary>
    /// T for affine
    /// </summary>
    public static readonly int affT = 17;
    /// <summary>
    /// Derivative affine K, computed as ModPow(k,-1)
    /// </summary>
    public static readonly int affKDerr = AlgoHelper.ModInverse(affK);
}

static class AlgoHelper
{
    /// <summary>
    /// Modulo multiplication of a and b
    /// </summary>
    /// <param name="a">first number</param>
    /// <param name="b">second number</param>
    public static int Mod(int a, int b) => ((a % CiphConstants.m) * (b % CiphConstants.m)) % CiphConstants.m;
    /// <summary>
    /// Modulo power of -1
    /// </summary>
    public static int ModInverse(int k) => Enumerable.Range(1, CiphConstants.m).First(i => i * k % CiphConstants.m == 1);
    /// <summary>
    /// Define if number is coprime to lenght of alphabet
    /// </summary>
    /// <param name="a">Key</param>
    public static bool IsCoprime(int a) => BigInteger.GreatestCommonDivisor(CiphConstants.m, a) == 1;
}

static class CaeserCipher
{
    public static void Encode(string input)
    {
        string result = "";
        for (int i = 0; i < input.Length; i++)
        {
            result += CiphConstants.alphabet[(CiphConstants.alphabet.IndexOf(input[i]) + CiphConstants.cesarK) % CiphConstants.m];
        }

        FileWorker.SaveFile(result.Insert(0,"Encoded\n"));
        Decode(result);
    }

    static void Decode(string input)
    {
        string result = "";
        for (int i = 0; i < input.Length; i++)
        {
            result += CiphConstants.alphabet[(CiphConstants.alphabet.IndexOf(input[i]) + CiphConstants.m - CiphConstants.cesarK) % CiphConstants.m];
        }

        FileWorker.SaveFile(result.Insert(0, "Decoded\n"));
    }
}

static class LinearCipher
{
    public static void Encode(string input)
    {
        if (!AlgoHelper.IsCoprime(CiphConstants.linearK)) throw new ArgumentException("One or more keys is not coprime");

        string result = "";
        for (int i = 0; i < input.Length; i++)
        {
            result += CiphConstants.alphabet[AlgoHelper.Mod(CiphConstants.alphabet.IndexOf(input[i]), CiphConstants.linearK)];
        }

        FileWorker.SaveFile(result.Insert(0, "Encoded\n"));
        Decode(result);
    }
    static void Decode(string input)
    {
        if (!AlgoHelper.IsCoprime(CiphConstants.linearK)) throw new ArgumentException("One or more keys is not coprime");

        string result = "";
        for (int i = 0; i < input.Length; i++)
        {
            int modRes = AlgoHelper.Mod(CiphConstants.linearKDerr, CiphConstants.alphabet.IndexOf(input[i]));
            result += CiphConstants.alphabet[modRes];   
        }

        FileWorker.SaveFile(result.Insert(0, "Decoded\n"));
    }
}

static class AffCipher
{
    public static void Encode(string input)
    {
        if (!AlgoHelper.IsCoprime(CiphConstants.affK)) throw new ArgumentException("One or more keys is not coprime");

        string result = "";
        for (int i = 0; i < input.Length; i++)
        {
            result += CiphConstants.alphabet[(AlgoHelper.Mod(CiphConstants.alphabet.IndexOf(input[i]), CiphConstants.affK) + CiphConstants.affT) % CiphConstants.m];
        }
  
        FileWorker.SaveFile(result.Insert(0, "Encoded\n"));
        Decode(result);
    }
    static void Decode(string input)
    {
        if (!AlgoHelper.IsCoprime(CiphConstants.affK)) throw new ArgumentException("One or more keys is not coprime");

        string result = "";
        for (int i = 0; i < input.Length; i++)
        {
            int modRes = (CiphConstants.affKDerr * (CiphConstants.alphabet.IndexOf(input[i]) - CiphConstants.affT)) % CiphConstants.m;
            result += CiphConstants.alphabet[(modRes < 0) ? CiphConstants.m + modRes : modRes];
        }
        FileWorker.SaveFile(result.Insert(0, "Decoded\n"));
    }
}

